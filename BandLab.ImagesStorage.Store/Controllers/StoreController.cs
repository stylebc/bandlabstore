﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using BandLab.ImageStorage.Store.Services;
using BandLab.ImagesStorage.Model.Messages;
using System.IO;

namespace BandLab.ImagesStorage.Store.Controllers
{
    [Route("api/[controller]")]
    public class StoreController : Controller
    {
        private readonly IStoreService _storeService;

        //// GET api/values
        //[HttpGet]
        //public IEnumerable<string> Get()
        //{
        //    return new string[] { "value1", "value2" };
        //}

        public StoreController(IStoreService storeService)
        {
            _storeService = storeService;
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public IActionResult Get(string id)
        {
            if (id == null)
                return Content("filename not present");

            var path = Path.Combine(Directory.GetCurrentDirectory(),
                           "wwwroot", id);
            if (!System.IO.File.Exists(path))
                return NotFound();
            FileContentResult result = new FileContentResult(System.IO.File.ReadAllBytes(path), GetContentType(path))
            {
                FileDownloadName = id
            };
            return result;

        }

        // POST api/values
        [HttpPost]
        public IActionResult Post([FromBody]SendFileRequest base64file)
        {
            if (base64file == null) return BadRequest();
            if (string.IsNullOrWhiteSpace(base64file.Base64FileContent)) return BadRequest();
            var filename = _storeService.SaveFile(base64file.Base64FileContent, base64file.FileExtension);
            return Ok(filename);
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public IActionResult Put(string id, [FromBody]SendFileRequest base64file)
        {
            if (base64file == null) return BadRequest();
            if (string.IsNullOrWhiteSpace(base64file.Base64FileContent)) return BadRequest();
            var filename = _storeService.UpdateFile(id, base64file.Base64FileContent, base64file.FileExtension);
            return Ok(id);
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(string id)
        {
            if (string.IsNullOrWhiteSpace(id)) return;
            _storeService.DeleteFile(id);
        }

        private string GetContentType(string path)
        {
            var types = GetMimeTypes();
            var ext = Path.GetExtension(path).ToLowerInvariant();
            return types[ext];
        }

        private Dictionary<string, string> GetMimeTypes()
        {
            return new Dictionary<string, string>
            {
                {".png", "image/png"},
                {".jpg", "image/jpeg"},
                {".jpeg", "image/jpeg"},
                {".gif", "image/gif"}
            };
        }
    }
}
