﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BandLab.ImagesStorage.Models
{
    public class CollectionCreateRequest
    {
        public string Name { get; set; }
        public IEnumerable<int> Images { get; set; }
    }
}
