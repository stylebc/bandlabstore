﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using BandLab.ImageStorage.Services;
using System.IO;
using BandLab.ImagesStorage.Model;
using System.Drawing;

namespace BandLab.ImagesStorage.Controllers
{
    [Route("api/[controller]")]
    public class ImagesController : Controller
    {
        private readonly IImagesService _imagesService;
        public ImagesController(IImagesService imagesService)
        {
            _imagesService = imagesService;
        }

        /// <summary>
        /// Get image metadata with Url
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // GET api/values/5
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var image = _imagesService.GetImage(id);
            if (image == null)
                return NotFound();
            image.ImageStoreID = ImageStoreService.storeURI + "/" + image.ImageStoreID;
            return Ok(image);
        }

        /// <summary>
        /// upload image
        /// </summary>
        /// <param name="file">image file</param>
        /// <returns>image id</returns>
        // POST api/values
        [HttpPost]
        public IActionResult Post(IFormFile file)
        {
            //TODO there no any extension checkups for this case, should be added
            if(file != null)
            {
                 var resultedId = _imagesService.AddImage(file);
                 return Ok(resultedId);                                
            }
            return BadRequest();
        }
        /// <summary>
        /// Update created image (aka replace)
        /// </summary>
        /// <param name="id">image id</param>
        /// <param name="file">uploaded file</param>
        /// <returns>Ok</returns>
        // PUT api/values/5
        [HttpPut("{id}")]
        public IActionResult Put(int id, IFormFile file)
        {
            if (file != null)
            {
                var result = _imagesService.UpdateImage(id, file);
                if(result) return Ok();
            }
            return NotFound();
        }

        /// <summary>
        /// Delete image from store
        /// </summary>
        /// <param name="id">image id</param>
        /// <returns></returns>
        // DELETE api/values/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var result = _imagesService.DeleteImage(id);
            if (result) return Ok();
            else return NotFound();
        }

        /// <summary>
        /// Read image metadata
        /// </summary>
        /// <param name="id">image id</param>
        /// <returns>image metadata</returns>
        [HttpGet("{id}/metadata")]
        public IActionResult GetMeta(int id)
        {
            var meta = _imagesService.GetImageMeta(id);
            if (meta == null) return NotFound();
            return Ok(meta);
        }
    }   
}
