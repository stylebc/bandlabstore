﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using BandLab.ImageStorage.Services;
using BandLab.ImagesStorage.Models;
using BandLab.ImagesStorage.Model;

namespace BandLab.ImagesStorage.Controllers
{
    [Route("api/[controller]")]
    public class CollectionController : Controller
    {
        private readonly IImagesService _imagesService;
        public CollectionController(IImagesService imagesService)
        {
            _imagesService = imagesService;
        }
        /// <summary>
        /// Get images with meta by collection id
        /// </summary>
        /// <param name="id">collection id</param>
        /// <returns>Images</returns>
        // GET api/collection/5
        [HttpGet("{id}")]
        public IEnumerable<BandImage> Get(int id)
        {            
            //TODO should be mapped properly
            var items = _imagesService.GetImagesByCollection(id).ToList();
            items.ForEach(p => p.ImageStoreID = ImageStoreService.storeURI + "/" + p.ImageStoreID);
            return items;                
        }

        /// <summary>
        /// Create new collection
        /// </summary>
        /// <param name="request">Collection name and initial images array</param>
        /// <returns>collection id</returns>
        // POST api/values
        [HttpPost]
        public IActionResult Post([FromBody]CollectionCreateRequest request)
        {
            if (request == null) return BadRequest();
            if (string.IsNullOrWhiteSpace(request.Name)) return BadRequest();
            if (request.Images == null) request.Images = new List<int>();
            var collectionId = _imagesService.CreateCollection(request.Name, request.Images.ToArray());
            return Ok(collectionId);
        }

        /// <summary>
        /// Update exists collection
        /// </summary>
        /// <param name="id">collection id</param>
        /// <param name="request">new name and initial images array</param>
        /// <returns>200 if all good</returns>
        // PUT api/collection/5        
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody]CollectionCreateRequest request)
        {
            if (request == null) return BadRequest();
            if (string.IsNullOrWhiteSpace(request.Name)) return BadRequest();
            if (request.Images == null) request.Images = new List<int>();
            var result = _imagesService.UpdateCollection(id, request.Name, request.Images.ToArray());
            if (!result) return BadRequest();
            return Ok();
        }

        /// <summary>
        /// Update collection name
        /// </summary>
        /// <param name="id">collection id</param>
        /// <param name="name">collection new name</param>
        /// <returns></returns>
        // PUT api/collection/5
        [HttpPut("{id}/name")]
        public IActionResult PutName(int id, [FromBody]string name)
        {
            
            if (string.IsNullOrWhiteSpace(name)) return BadRequest();
            
            var result = _imagesService.UpdateCollectionName(id, name);
            if (!result) return BadRequest();
            return Ok();
        }

        /// <summary>
        /// Delete collection. Images in collection are not deleted, they can be assigned to many collections
        /// </summary>
        /// <param name="id">collection id</param>
        /// <returns></returns>
        // DELETE api/values/5        
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var result = _imagesService.RemoveCollection(id);
            if (!result) return BadRequest();
            return Ok();
        }
    }   
}
