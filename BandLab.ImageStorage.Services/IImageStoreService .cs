﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BandLab.ImageStorage.Services
{
    public interface IImageStoreService
    {
        /// <summary>
        /// Call POST of store Rest Api
        /// </summary>
        /// <param name="base64">file content</param>
        /// <param name="extension">file extension</param>
        /// <returns>generated filename</returns>
        string SaveToStore(string base64, string extension);
        /// <summary>
        /// Call DELETE of store Rest Api
        /// </summary>
        /// <param name="filename">file to delete</param>
        void DeleteFromStore(string filename);
        /// <summary>
        /// Update (Replace) file
        /// </summary>
        /// <param name="filename">old filename</param>
        /// <param name="base64">content</param>
        /// <param name="extension">file extension</param>
        /// <returns>new filename</returns>
        string UpdateFileInStore(string filename, string base64, string extension);
    }
}
