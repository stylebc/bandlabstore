﻿using BandLab.ImagesStorage.Data.Infrastructure;
using BandLab.ImagesStorage.Data.Repositories;
using BandLab.ImagesStorage.Model;
using BandLab.ImagesStorage.Model.Messages;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;

namespace BandLab.ImageStorage.Services
{
    public class ImagesService: IImagesService
    {
        private readonly IImagesRepository _imagesRepository;
        private readonly IImageCollectionRepository _collectionsRepository;
        private readonly IImageStoreService _storeService;
        private readonly IUnitOfWork _unitOfWork;

        private const int ExtensionLength = 3;

        public ImagesService(IImagesRepository imagesRepository, IImageCollectionRepository collectionsRepository, IImageStoreService storeService, IUnitOfWork unitOfWork)
        {
            _imagesRepository = imagesRepository;
            _collectionsRepository = collectionsRepository;
            _storeService = storeService;
            _unitOfWork = unitOfWork;
        }

        public IEnumerable<BandImage> GetImages()
        {
            var images = _imagesRepository.GetAll();
            return images;
        }

        public BandImage GetImage(int id)
        {
            var image = _imagesRepository.GetByIdWithMeta(id);
            return image;
        }

        public int AddImage(IFormFile imageFile)
        {
            using (var stream = new MemoryStream())
            {
                imageFile.CopyTo(stream);                
                
                var base64 = Convert.ToBase64String(stream.ToArray());

                var bandimage = new BandImage()
                {
                    Name = imageFile.FileName                    
                };
                var img = new Bitmap(stream);
                var meta = new BandImageMeta();               
                FillMetadata(meta, imageFile.FileName, imageFile.Length, img.Width, img.Height);
                bandimage.Meta = meta;
                _imagesRepository.Add(bandimage);

                bandimage.ImageStoreID = _storeService.SaveToStore(base64, meta.Extension);
                Commit();
                var resultedId = bandimage.ID;
                

                return resultedId;
            }                                                
        }               

        public void Commit()
        {
            _unitOfWork.Commit();
        }

        public bool UpdateImage(int imageId, IFormFile imageFile)
        {
            var image = GetImage(imageId);
            if (image == null) return false;

            using (var stream = new MemoryStream())
            {
                imageFile.CopyTo(stream);

                var base64 = Convert.ToBase64String(stream.ToArray());
                image.Name = imageFile.FileName;               
                var img = new Bitmap(stream);                
                FillMetadata(image.Meta, imageFile.FileName, imageFile.Length, img.Width, img.Height);
                image.ImageStoreID = _storeService.UpdateFileInStore(image.ImageStoreID, base64, image.Meta.Extension);
                Commit();
                return true;
            }
        }

        private void FillMetadata(BandImageMeta meta, string filename,long size, int width, int height)
        {
            meta.Resolution = $"{width}x{height}";
            meta.Size = size;
            meta.Timestamp = DateTime.UtcNow;
            meta.Extension = filename.Substring(filename.Length - ExtensionLength);            
        }

        public BandImageMeta GetImageMeta(int imageId)
        {
            return _imagesRepository.GetImageMeta(imageId);
        }

        public bool DeleteImage(int imageId)
        {
            var image = GetImage(imageId);
            if (image == null) return false;
            _imagesRepository.Delete(image);
            _storeService.DeleteFromStore(image.ImageStoreID);
            Commit();
            return true;
        }

        public int CreateCollection(string name, int[] imageIds)
        {
            var collection = new BandImageCollection { Name = name };
            _collectionsRepository.Add(collection);
            Commit();
            var imagesToAdd = _imagesRepository.GetByIds(imageIds).Select(p => new BandImageCollectionBandImage { BandImageID = p.ID, BandImageCollectionID = collection.ID }).ToList();
            collection.ImageCollections = imagesToAdd;
            Commit();
            return collection.ID;

        }

        public bool UpdateCollection(int id, string name, int[] imageIds)
        {
            var result = UpdateCollectionName(id, name, false);
            if (!result) return false;
            _collectionsRepository.UpdateCollectionImages(id, imageIds);
            Commit();
            return true;
        }

        public bool UpdateCollectionName(int id, string name, bool commit = true)
        {
            var collection = _collectionsRepository.Get(p => p.ID == id);
            if (collection == null) return false;
            if (string.IsNullOrWhiteSpace(name)) return false;
            collection.Name = name;
            if (commit)
                Commit();
            return true;
        }

        public IEnumerable<BandImage> GetImagesByCollection(int collectionId)
        {
            return _imagesRepository.GetByCollection(collectionId);
        }

        public bool RemoveImageFromCollection(int collectionId, int imageId)
        {
           var result = _collectionsRepository.RemoveImageFromCollection(collectionId, imageId);
            Commit();
            return result;
        }

        public bool AddImageToCollection(int collectionId, int imageId)
        {
            var result = _collectionsRepository.AddImageToCollection(collectionId, imageId);
            Commit();
            return result;
        }

        public bool RemoveCollection(int collectionId)
        {
            var result = _collectionsRepository.RemoveCollection(collectionId);
            Commit();
            return result;
        }
    }
}
