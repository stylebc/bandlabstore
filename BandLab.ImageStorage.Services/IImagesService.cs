﻿using BandLab.ImagesStorage.Model;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;

namespace BandLab.ImageStorage.Services
{
    public interface IImagesService
    {
        IEnumerable<BandImage> GetImages();
        BandImage GetImage(int id);
        int AddImage(IFormFile imageFile);
        bool UpdateImage(int imageId, IFormFile imageFile);
        BandImageMeta GetImageMeta(int imageId);
        bool DeleteImage(int imageId);

        int CreateCollection(string name, int[] imageIds);
        IEnumerable<BandImage> GetImagesByCollection(int collectionId);
        bool UpdateCollectionName(int id, string name, bool commit = true);
        bool UpdateCollection(int id, string name, int[] imageIds);

        bool RemoveImageFromCollection(int collectionId, int imageId);
        bool AddImageToCollection(int collectionId, int imageId);
        bool RemoveCollection(int collectionId);
        
        void Commit();
    }
}
