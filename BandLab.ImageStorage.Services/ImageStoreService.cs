﻿using BandLab.ImagesStorage.Model.Messages;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace BandLab.ImageStorage.Services
{
    public class ImageStoreService: IImageStoreService  
    {
        //TODO move to app.config
        //TODO there no checkups for bad request 
        public static string storeURI = "http://localhost:55800/api/store";

        public string SaveToStore(string base64, string extension)
        {
            var file = new SendFileRequest()
            {
                Base64FileContent = base64,
                FileExtension = extension
            };


            var result = string.Empty;
            using (var client = new WebClient())
            {
                client.Headers[HttpRequestHeader.ContentType] = "application/json";
                result = client.UploadString(storeURI, "POST", JsonConvert.SerializeObject(file));
            }
            return result;
        }

        public string UpdateFileInStore(string filename, string base64, string extension)
        {
            var file = new SendFileRequest()
            {
                Base64FileContent = base64,
                FileExtension = extension
            };


            var result = string.Empty;
            using (var client = new WebClient())
            {
                client.Headers[HttpRequestHeader.ContentType] = "application/json";
                result = client.UploadString(storeURI+"/"+filename, "PUT", JsonConvert.SerializeObject(file));
            }
            return result;
        }

        public void DeleteFromStore(string filename)
        {
            var result = string.Empty;
            using (var client = new WebClient())
            {
                client.Headers[HttpRequestHeader.ContentType] = "application/json";
                result = client.UploadString(storeURI+"/"+filename, "DELETE", string.Empty);
            }            
        }
    }
}
