﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BandLab.ImagesStorage.Model
{
    public class BandImageMeta
    {
        public int ID { get; set; }
        public int BandImageID { get; set; }
        public string Resolution { get; set; }
        public string Extension { get; set; }
        public long Size { get; set; }        
        public DateTime Timestamp { get; set; }
    }
}
