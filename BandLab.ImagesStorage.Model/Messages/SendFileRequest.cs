﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BandLab.ImagesStorage.Model.Messages
{
    public class SendFileRequest
    {
        public string Base64FileContent { get; set; }
        public string FileExtension { get; set; }
    }
}
