﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BandLab.ImagesStorage.Model
{
    public class BandImageCollection
    {
        public int ID { get; set; }
        public string Name { get; set; }        
        public ICollection<BandImageCollectionBandImage> ImageCollections { get; set; }
    }
}
