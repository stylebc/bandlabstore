﻿using System;
using System.Collections.Generic;

namespace BandLab.ImagesStorage.Model
{
    public class BandImage
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public BandImageMeta Meta { get; set; } 
        public string ImageStoreID { get; set; }
        public ICollection<BandImageCollectionBandImage> ImageCollections { get; set; }
    }
}
