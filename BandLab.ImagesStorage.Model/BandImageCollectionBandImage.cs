﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BandLab.ImagesStorage.Model
{
    public class BandImageCollectionBandImage
    {
        public int BandImageID { get; set; }
        public BandImage BandImage { get; set; }

        public int BandImageCollectionID { get; set; }        
        public BandImageCollection BandImageCollection { get; set; }
    }
}
