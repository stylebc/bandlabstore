﻿using BandLab.ImagesStorage.Model;
using Microsoft.EntityFrameworkCore;

using System;

namespace BandLab.ImagesStorage.Data
{
    public class BandLabContext : DbContext
    {
        public DbSet<BandImage> Images { get; set; }
        public DbSet<BandImageCollection> ImageCollections { get; set; }
        public DbSet<BandImageMeta> Metas { get; set; }
        public DbSet<BandImageCollectionBandImage> BandImageCollectionBandImages { get; set; }

        public virtual void Commit()
        {
             base.SaveChanges();                       
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.Entity<BandImage>().ToTable("BandImages");
            modelBuilder.Entity<BandImageCollection>().ToTable("BandImageCollections");
            modelBuilder.Entity<BandImageMeta>().ToTable("BandImageMetas");
            modelBuilder.Entity<BandImageCollectionBandImage>().ToTable("BandImageCollectionBandImage");



            modelBuilder.Entity<BandImageCollectionBandImage>()
                .HasKey(c => new { c.BandImageID, c.BandImageCollectionID });
            modelBuilder.Entity<BandImageCollectionBandImage>()
                .HasOne(p => p.BandImage)
                .WithMany(p => p.ImageCollections).HasForeignKey(p => p.BandImageID);
            modelBuilder.Entity<BandImageCollectionBandImage>()
                .HasOne(p => p.BandImageCollection)
                .WithMany(p => p.ImageCollections).HasForeignKey(p => p.BandImageCollectionID);



        }

        public BandLabContext(DbContextOptions<BandLabContext> options)
            : base(options)
        {
        }
    }
}
