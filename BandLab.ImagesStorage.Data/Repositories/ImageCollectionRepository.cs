﻿using BandLab.ImagesStorage.Data.Infrastructure;
using BandLab.ImagesStorage.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BandLab.ImagesStorage.Data.Repositories
{
    public class ImageCollectionRepository : RepositoryBase<BandImageCollection>, IImageCollectionRepository
    {
        public ImageCollectionRepository(BandLabContext context) : base(context)
        {
        }

        public bool AddImageToCollection(int collectionId, int imageId)
        {
            var collection = GetById(collectionId);
            if (collection == null) return false;
            var exist = Context.BandImageCollectionBandImages.Any(p => p.BandImageCollectionID == collectionId && p.BandImageID == imageId);
            if (exist)
            {
                return true;
            }
            var image = Context.Images.FirstOrDefault(p => p.ID == imageId);
            if (image == null) return false;
            Context.BandImageCollectionBandImages.Add(
                new BandImageCollectionBandImage { BandImageCollectionID = collectionId, BandImageID = imageId });
            return true;
        }

        public bool RemoveCollection(int collectionId)
        {
            var collection = GetById(collectionId);
            if (collection == null) return false;
            var collectionItems = Context.BandImageCollectionBandImages.Where(p => p.BandImageCollectionID == collectionId).ToList();
            Context.BandImageCollectionBandImages.RemoveRange(collectionItems);
            Context.ImageCollections.Remove(collection);
            return true;
        }

        public bool RemoveImageFromCollection(int collectionId, int imageId)
        {
            var collection = GetById(collectionId);
            if (collection == null) return false;
            var item = Context.BandImageCollectionBandImages.FirstOrDefault(p => p.BandImageCollectionID == collectionId && p.BandImageID == imageId);
            if (item == null) return false;
            Context.BandImageCollectionBandImages.Remove(item);
            return true;
        }

        public void UpdateCollectionImages(int collectionId, int[] imagesIds)
        {
            var currentItems = Context.BandImageCollectionBandImages.Where(p => p.BandImageCollectionID == collectionId).ToList();

            foreach(var item in currentItems)
            {
                if (!imagesIds.Contains(item.BandImageID))
                {
                    Context.BandImageCollectionBandImages.Remove(item);
                }
            }
            var currentIds = currentItems.Select(p => p.BandImageID).ToList();
            foreach (var id in imagesIds.Where(i => !currentIds.Contains(i)))
            {
                Context.BandImageCollectionBandImages.Add(new BandImageCollectionBandImage { BandImageCollectionID = collectionId, BandImageID = id });
            }            
        }
    }

    public interface IImageCollectionRepository : IRepository<BandImageCollection>
    {
        void UpdateCollectionImages(int collectionId, int[] imagesIds);
        bool RemoveImageFromCollection(int collectionId, int imageId);
        bool AddImageToCollection(int collectionId, int imageId);
        bool RemoveCollection(int collectionId);
    }
}
