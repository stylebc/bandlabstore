﻿using BandLab.ImagesStorage.Data.Infrastructure;
using BandLab.ImagesStorage.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BandLab.ImagesStorage.Data.Repositories
{
    public class ImagesRepository: RepositoryBase<BandImage>, IImagesRepository
    {
        public ImagesRepository(BandLabContext context) : base(context)
        {
        }

        public override IEnumerable<BandImage> GetAll()
        {
            return Context.Images.ToList();
        }

        public IEnumerable<BandImage> GetByCollection(int collectionId)
        {
            return Context.BandImageCollectionBandImages.Where(p => p.BandImageCollectionID == collectionId)
                .Select(p => p.BandImage).Include(p=>p.Meta).ToList();
        }

        public IEnumerable<BandImage> GetByIds(int[] ids)
        {
            return Context.Images.Where(p => ids.Contains(p.ID)).ToList();
        }

        public BandImage GetByIdWithMeta(int id)
        {
            return Context.Images.Include(p => p.Meta).FirstOrDefault(p => p.ID == id);
        }

        public BandImageMeta GetImageMeta(int imageId)
        {
            return Context.Metas.FirstOrDefault(p => p.BandImageID == imageId);
        }
    }

    public interface IImagesRepository : IRepository<BandImage>
    {
        BandImage GetByIdWithMeta(int id);
        BandImageMeta GetImageMeta(int imageId);
        IEnumerable<BandImage> GetByIds(int[] ids);
        IEnumerable<BandImage> GetByCollection(int collectionId);
    }
}
