﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BandLab.ImagesStorage.Data.Infrastructure
{
    public interface IUnitOfWork
    {
        void Commit();
    }
}
