﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BandLab.ImagesStorage.Data.Infrastructure
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly BandLabContext _context;
        public UnitOfWork(BandLabContext context)
        {
            _context = context;
        }

        public BandLabContext DbContext
        {
            get { return _context; }
        }

        public void Commit()
        {
            DbContext.Commit();
        }
    }
}
