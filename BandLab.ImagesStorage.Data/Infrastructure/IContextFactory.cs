﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BandLab.ImagesStorage.Data.Infrastructure
{
    public interface IContextFactory : IDisposable
    {
        BandLabContext Init();
    }
}
