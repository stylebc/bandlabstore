﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BandLab.ImagesStorage.Data
{
    public class DbInitializer
    {
        public static void Initialize(BandLabContext context)
        {
            context.Database.EnsureCreated();
        }
    }
}
