﻿using System;
using System.IO;

namespace BandLab.ImageStorage.Store.Services
{
    public interface IStoreService
    {
        /// <summary>
        /// Save file content to store
        /// </summary>
        /// <param name="base64content">Base64 encoded filebody</param>
        /// <param name="extension">file extension</param>
        /// <returns>filename without extension</returns>
        string SaveFile(string base64content, string extension);

        /// <summary>
        /// Delete file by name if exist
        /// </summary>
        /// <param name="filename">file to delete</param>
        void DeleteFile(string filename);

        /// <summary>
        /// Deletes exists file in store and creates new one
        /// </summary>
        /// <param name="filename">file to update</param>
        /// <param name="base64content">Base64 encoded filebody</param>
        /// <param name="extension">file extension</param>
        /// <returns>new Filename</returns>
        string UpdateFile(string filename, string base64content, string extension);
    }
}
