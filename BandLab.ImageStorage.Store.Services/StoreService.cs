﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace BandLab.ImageStorage.Store.Services
{
    public class StoreService: IStoreService
    {
        public void DeleteFile(string filename)
        {
            var path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", filename);
            if (File.Exists(path))
                File.Delete(path);
        }

        public string SaveFile(string base64content, string extension)
        {
            var bytes = Convert.FromBase64String(base64content);

            var filename = $"{Guid.NewGuid()}.{extension}";

            var path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", filename);

            using (var stream = new FileStream(path, FileMode.Create))
            {
                stream.Write(bytes, 0, bytes.Length);
            }

            return filename;
        }

        public string UpdateFile(string filename, string base64content, string extension)
        {
            DeleteFile(filename);
            return SaveFile(base64content, extension);
        }
    }
}
